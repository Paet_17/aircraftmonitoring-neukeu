import mysql.connector


cnx = mysql.connector.connect(user="root", password="password",
                              host="localhost",
                              database="aircraft")

cursor = cnx.cursor()

#SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
def get_max_speed():
    query = ("SELECT seentime, MAX(speed) FROM DUMP1090DATA")
    cursor.execute(query)
    return cursor.fetchone()

def get_max_height():
    query = ("SELECT seentime, MAX(height) FROM DUMP1090DATA")
    cursor.execute(query)
    return cursor.fetchone()



print get_max_height()
print get_max_speed()