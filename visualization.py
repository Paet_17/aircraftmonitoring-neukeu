import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import datetime as dt
import aircraft2 as aircraft
import matplotlib.image as mpimg
from matplotlib.gridspec import GridSpec
import random
from PIL import Image

fig = plt.figure(figsize=(12.8, 10.24), dpi=300, facecolor='black')
gs = GridSpec(3, 4)


ax = fig.add_subplot(111,facecolor='black')

ax.text(0, 1.03, "Allzeit Extremwerte", fontsize=17, color='white',weight="bold")

ax.text(0, 0.97, "Meistbeflogener Tag: {0}".format(aircraft.get_max_date()), fontsize=12, color='white',alpha=0.8)
ax.text(0, 0.92, "Meistbeflogene Nacht: {0}".format(aircraft.get_max_date_night()), fontsize=12, color='white',alpha=0.8)
ax.text(0, 0.87, "Meistbeflogener Sonntag: {0}".format(aircraft.get_max_date_sunday()), fontsize=12, color='white',alpha=0.8)
ax.text(0, 0.82, "Hoechstgemessene Geschwindigkeit: {0} km/h".format(aircraft.get_max_speed()[1]), fontsize=12, color='white',alpha=0.8)
ax.text(0, 0.77, "Hoechstgemessene Flughoehe: {0} m".format(aircraft.get_max_height()[1]), fontsize=12, color='white',alpha=0.8)
ax.text(0, 0.72, "Durchschnittle Fluege am Sonntag: {0}".format(aircraft.get_avg_sun_IBK()), fontsize=12, color='white',alpha=0.8)


ax.text(0.55, 1.03, "Tageswerte", fontsize=17, color='white',weight="bold")
ax.text(0.55, 0.97, "Flugzeuge heute: {0}".format(aircraft.get_number_today()), fontsize=12, color='white',alpha=0.8)
ax.text(0.55, 0.92, "Hoechstgemessene Geschwindigkeit: {0} km/h".format(aircraft.get_max_speed()[1]), fontsize=12, color='white',alpha=0.8)
ax.text(0.55, 0.87, "Hoechstgemessene Flughoehe: {0} m".format(aircraft.get_max_height()[1]), fontsize=12, color='white',alpha=0.8)
plt.subplots_adjust(wspace=0.5,)

daily30=fig.add_subplot(gs[1, :-2])
daily30.patch.set_alpha(0)
daily30.plot(range(30),aircraft.get_30days())
daily30.set_title("Flugzeuge pro Tag",color="red")
daily30.yaxis.label.set_color('red')
daily30.tick_params(axis='x', colors='red')
daily30.tick_params(axis='y', colors='red')
daily30.spines['left'].set_color('red')
daily30.spines['bottom'].set_color('red')
daily30.set_ylabel("Anzahl der Fluege")
daily30.set_xticks(range(1,31,2))

daily_night_30=fig.add_subplot(gs[1, 2:])
daily_night_30.patch.set_alpha(0)
daily_night_30.set_title("Flugzeuge zw 22:00 - 6:00",color="red")
daily_night_30.bar(range(30),aircraft.get_nightflights_30days())
daily_night_30.yaxis.label.set_color('red')
daily_night_30.tick_params(axis='x', colors='red')
daily_night_30.tick_params(axis='y', colors='red')
daily_night_30.spines['left'].set_color('red')
daily_night_30.spines['bottom'].set_color('red')
daily_night_30.set_ylabel("Anzahl der Fluege")
daily_night_30.set_xticks(range(1,31,2))


ax.text(-0.07, 0.25, "Datum", fontsize=17, color='white',weight="bold")
ax.text(0.1, 0.25, "Flugnummer", fontsize=17, color='white',weight="bold")
ax.text(0.33, 0.25, "SQWAK", fontsize=17, color='white',weight="bold")
ax.text(0.52, 0.25, "Bedeutung", fontsize=17, color='white',weight="bold")
ax.text(0.88, 0.25, "Bild", fontsize=17, color='white',weight="bold")

import urllib2
import json
contents = urllib2.urlopen("https://v4p4sz5ijk.execute-api.us-east-1.amazonaws.com/anbdata/airlines/designators/code-list?api_key=9304aa50-2c5c-11e8-b618-9f9872b1f8e5&format=json&states=&operators=CFG").read()
test = json.loads(contents)
print test
#API
#https://v4p4sz5ijk.execute-api.us-east-1.amazonaws.com/anbdata/airlines/designators/code-list?api_key=9304aa50-2c5c-11e8-b618-9f9872b1f8e5&format=json&states=&operators=DLH,AUA

#SQL
#SELECT count(DISTINCT flightnr) as c,SUBSTRING(flightnr,1,3) FROM DUMP1090DATA group by SUBSTRING(flightnr,1,3) order by c DESC LIMIT 10;

pic_int = random.randint(0,4)

for i in range(0,5):
    
    color = 'white'
    if i==pic_int:
        color='red'

    ax.text(-0.11, 0.20-0.05*i, "22.02.2018 13:25", fontsize=12, color=color,weight="bold",alpha=0.8)
    ax.text(0.15, 0.20-0.05*i, "ALPIN2", fontsize=12, color=color,weight="bold",alpha=0.8)
    ax.text(0.36, 0.20-0.05*i, "1657", fontsize=12, color=color,weight="bold",alpha=0.8)
    ax.text(0.52, 0.20-0.05*i, "Christophorus Heli", fontsize=12, color=color,weight="bold",alpha=0.8)

implot=fig.add_subplot(gs[2, 3:])
img = Image.open('alpin2.jpg')
img.thumbnail((230, 230), Image.ANTIALIAS)  # resizes image in-place
implot.imshow(img)

ax.text(0.85, -0.1, "created " + dt.datetime.now().strftime("%Y-%m-%d %H:%M"), fontsize=10, color='white', alpha=0.25)
fig.savefig("table.png", facecolor=fig.get_facecolor())