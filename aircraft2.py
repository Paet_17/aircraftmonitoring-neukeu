import mysql.connector
import datetime
import time
import holidays
import configparser
import os

os.chdir(str(os.path.dirname(os.path.abspath(__file__))))
config = configparser.ConfigParser()
config.read('config.ini')
output_path=config.get('main', 'PATH_GRAPH_OUTPUT')


#SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));


cnx = mysql.connector.connect(user=config.get('main', 'DB_USERNAME'), password=config.get('main', 'DB_PASSWORD'),
                              host=config.get('main', 'DB_HOSTNAME_NAME'),
                              database=config.get('main', 'DB_NAME'))

cursor = cnx.cursor()


def get_number_today():
    date = datetime.datetime.now()
    date = date.replace(hour=0, minute=0, second=0)
    date = time.mktime(date.timetuple())
    date = str(date)
    com = "SELECT count(distinct(flightnr)) from DUMP1090DATA where seentime >" + date + ";"
    query = (com)
    cursor.execute(query)
    return cursor.fetchone()[0]


def get_avg_sun_IBK():
    query = ("select  count(distinct(flightnr))  from DUMP1090DATA"
            +" where DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday' and (lat  between 47.252998 and 47.266798)"
            + " and (lon between 11.326835 and 11.367928) and (height < 10000);")
    cursor.execute(query)
    anzSunFlights = cursor.fetchone()
    query = ("select count(Distinct(DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W - %D - %M - %Y')))  from DUMP1090DATA"
            + " where DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday'"
            +" and (lat  between 47.252998 and 47.266798) and"
            +" (lon between 11.326835 and 11.367928)"
            + " and (height < 10000);")
    cursor.execute(query)
    anzSun = cursor.fetchone()
    return (anzSunFlights[0] / anzSun[0])


def get_ts_30days():
    date = datetime.datetime.now() + datetime.timedelta(-30)
    return time.mktime(date.timetuple())


def get_ts_daystart(timestamp):
    date = datetime.datetime.fromtimestamp(timestamp)
    start = date.replace(hour=0, minute=0, second=0)
    return time.mktime(start.timetuple())


def get_ts_dayend(timestamp):
    date = datetime.datetime.fromtimestamp(timestamp)
    end = date.replace(hour=23, minute=59, second=59)
    return time.mktime(end.timetuple())


def get_1d_later(timestamp):
    date = datetime.datetime.fromtimestamp(timestamp)
    date = date + datetime.timedelta(1)
    return time.mktime(date.timetuple())


def get_ts_22(timestamp):
    date = datetime.datetime.fromtimestamp(timestamp)
    date = date.replace(hour=22, minute=0, second=0)
    return time.mktime(date.timetuple())


def get_ts_06(tiemstamp):
    date = datetime.datetime.fromtimestamp(tiemstamp)
    date = date.replace(hour=6, minute=0, second=0)
    return time.mktime(date.timetuple())


def get_ts_first_data():
    query = ("select min(seentime) from DUMP1090DATA;")
    cursor.execute(query)
    return cursor.fetchone()[0]


def write_holydays_table():
    endy =  datetime.date.today().year
    starty = datetime.datetime.fromtimestamp(get_ts_first_data()).year
    holys = holidays.AT(years=range(starty, endy+1), prov='T')
    query = ("create table if not exists holidays( "
            + "day date primary key, "
            + "name varchar(100)); ")
    cursor.execute(query)
    cnx.commit()
    query = ""
    for date, name in sorted(holys.items()):
        query = ("insert into holidays values ( '" + str(date) + "', '" + str(name) + "' ) "
            + " ON DUPLICATE KEY UPDATE day = day;")
        cursor.execute(query)
    cnx.commit()


def get_30days():
    date = get_ts_30days()
    start = get_ts_daystart(date)
    end = get_ts_dayend(start)
    l = []
    for i in range(0,30):
        starts = str(start)
        ends = str(end)
        query = ("select count(distinct(flightnr)) from DUMP1090DATA where seentime between " + starts + " and " + ends + ";")
        cursor.execute(query)
        l.append(cursor.fetchone()[0])
        start = get_1d_later(start)
        end = get_1d_later(end)
    return l


def get_nightflights_30days():
    date = get_ts_30days()
    start = get_ts_22(date)
    end = get_ts_06(start)
    l = []
    for i in range(0,30):
        starts = str(start)
        ends = str(end)
        query = ("select count(distinct(flightnr)) from DUMP1090DATA"
                +" where (lat  between 47.252998 and 47.266798) and"
                +" (lon between 11.326835 and 11.367928) and"
                +" (height < 10000) and"
                +" (seentime between " +starts + " and " + ends +");")
        cursor.execute(query)
        l.append(cursor.fetchone()[0])
        start = get_1d_later(start)
        end = get_1d_later(end)
    return l


def get_max_date():
    query =("select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "group by datum "
            + "having count = (select MAX(A.COUNT) "
            + "from ( "
            + "select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "group by datum ) AS A);")
    cursor.execute(query)
    return  cursor.fetchone()[0]


def get_max_date_night():
    query =("select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "where TIME(FROM_UNIXTIME(seentime)) not between '06:00:00' and '22:00:00' "
            + "group by datum "
            + "having count = (select MAX(A.COUNT) "
            + "from ( "
            + "select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "where TIME(FROM_UNIXTIME(seentime)) not between '06:00:00' and '22:00:00' "
            + "group by datum ) AS A);")
    cursor.execute(query)
    return  cursor.fetchone()[0]


def get_max_date_sunday():
    query =("select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "where DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday' "
            + "group by datum "
            + "having count = (select MAX(A.COUNT) "
            + "from ( "
            + "select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from DUMP1090DATA "
            + "where DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday' "
            + "group by datum ) AS A);")
    cursor.execute(query)
    return  cursor.fetchone()[0]


def get_max_speed():
    query = ("SELECT seentime, MAX(speed) FROM DUMP1090DATA")
    cursor.execute(query)
    return cursor.fetchone()


def get_max_height():
    query = ("SELECT seentime, MAX(height) FROM DUMP1090DATA")
    cursor.execute(query)
    return cursor.fetchone()


def get_avg_holiday():
    query = ("SELECT COUNT(DISTINCT(flightnr)) FROM DUMP1090DATA "
            + "WHERE (lat  between 47.252998 and 47.266798) and(lon between 11.326835 and 11.367928) "
            +  " and (height < 10000) AND Date(From_UNIXTIME(seentime)) in (select day from holidays);")
    cursor.execute(query)
    flights = cursor.fetchone()[0]
    query = ("SELECT count(DISTINCT(holidays.day)) FROM holidays "
            + "WHERE holidays.day IN ( SELECT Date(From_UNIXTIME(seentime)) FROM DUMP1090DATA);")
    cursor.execute(query)
    sundays = cursor.fetchone()[0]
    return flights/sundays


def get_max_date_holiday():
    query = ("select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from dump1090data WHERE Date(From_UNIXTIME(seentime)) in (select day from holidays) "
            + "group by datum having count = (select MAX(A.COUNT) from ( "
            + "select Date(FROM_UNIXTIME(seentime)) as datum, count(distinct(flightnr)) as count "
            + "from dump1090data WHERE Date(From_UNIXTIME(seentime)) in (select day from holidays) "
            + "group by datum) AS A) ")
    cursor.execute(query)
    return cursor.fetchone()[0]


def get_avg_night_nonworkday():
    query = ("SELECT count(DISTINCT(flightnr)) FROM DUMP1090DATA "
            + "WHERE (lat  between 47.252998 and 47.266798) AND (lon between 11.326835 and 11.367928) "
            + "AND (height < 10000) AND (TIME(FROM_UNIXTIME(seentime)) not between "
            + "'06:00:00' and '22:00:00') AND ((Date(From_UNIXTIME(seentime)) in "
            + "(select day from holidays)) OR (DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday'))")
    cursor.execute(query)
    flights = cursor.fetchone()[0]
    query = ("SELECT COUNT(DISTINCT(DATE(FROM_UNIXTIME(seentime)))) FROM DUMP1090DATA "
            + "WHERE (TIME(FROM_UNIXTIME(seentime)) not between '06:00:00' and '22:00:00') AND "
            + "((Date(From_UNIXTIME(seentime)) in (select day from holidays)) OR "
            + "(DATE_FORMAT(FROM_UNIXTIME(`seentime`), '%W') = 'Sunday'));")
    cursor.execute(query)
    days = cursor.fetchone()[0]
    return float(flights)/float(days)


#wieviel Flugzeuge heute ("gesehene") empfangen als Zahl
# print get_number_today()
# #wieviele Flugzeuge tglich empfangen ("gesehene") (letzten 30 tage)
# print get_30days()
# #wieviel Flugzeuge in Innsbruck gestartet gelandet zwischen 22.00 und 06.00(30 Tage)
# print get_nightflights_30days()
# #wieviel Flugzeuge in Innsbruck gestartet gelandet an SO Durschnitt ueber alle Daten (als Zahl)
# print get_avg_sun_IBK()
# #welches Datum mit meisten gesehenen Fluegen,
# print get_max_date()
# # welche Nachtzeit,
# print get_max_date_night()
#welcher Sonntag,
#print get_max_date_sunday()
#welcher Feiertag
#print get_max_date_holiday()
#Altzeitextremwerte
# print get_max_speed()
# print get_max_height()
#wieviel Flugzeuge in Innsbruck gestartet gelandet an Feiertag Durschnitt ueber alle Daten (als Zahl)
#print get_avg_holiday()
#wieviel Flugzeuge in Innsbruck gestartet gelandet -
#  zwischen 22.00 und 06.00 und (SO oder Feiertag) als Durschnitt ueber alle Daten (als Zahl)
#print get_avg_night_nonworkday()
#developement...


write_holydays_table()